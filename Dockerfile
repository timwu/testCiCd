FROM ubuntu:16.04
MAINTAINER timwu<timwu@gmail.com>

RUN \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y \
    python \
    python-pip \
    python3 \
    python3-pip \
    sshpass \
    openssh-server

RUN mkdir -p  /var/run/sshd && \
    echo 'root:root' | chpasswd && \
    sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd && \
    echo "export VISIBLE=now" >> /etc/profile
ENV NOTVISIBLE "in users profile"

RUN pip install \
    flask \
    flask-script \
    ansible \
    pyinstaller

ENV HOME /root
EXPOSE 5000 22
WORKDIR /root

CMD ["/usr/sbin/sshd", "-D"]
